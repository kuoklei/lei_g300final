﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private string moveInputAxis = "Vertical";
    private string turnInputAxis = "Horizontal";

    Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;

    public float rotationRate;

    public float moveSpeed;

    private AudioSource moveSound;
    private bool walking = false;

    private void Awake()
    {
        anim = GetComponent<Animator>();                    // Reference to the animator component.
        playerRigidbody = GetComponent<Rigidbody>();
        moveSound = GetComponent<AudioSource>();
    }

    private void Update()
    {
        float moveAxis = Input.GetAxis(moveInputAxis);
        float turnAxis = Input.GetAxis(turnInputAxis);
        float hAxis = Input.GetAxis("Horizontal");
        float xAxis = Input.GetAxis("Vertical");
        anim.SetBool("isWalking", false);
        
        //player walk in four directions
        if (hAxis > 0 || hAxis < 0)
        {
            Vector3 movement = transform.right*hAxis * moveSpeed * Time.deltaTime;
            movement += transform.forward * xAxis * moveSpeed * Time.deltaTime;
            playerRigidbody.MovePosition(transform.position + movement);
            if (walking == false)
            {
                moveSound.Play();
                walking = true;
            }
            anim.SetBool("isWalking", true);
        }

        if (xAxis > 0 || xAxis < 0)
        {
            
            Vector3 movement = transform.right * hAxis * moveSpeed * Time.deltaTime;
            movement += transform.forward * xAxis * moveSpeed * Time.deltaTime;
            playerRigidbody.MovePosition(transform.position + movement);
            moveSound.Play();
            anim.SetBool("isWalking", true);
            if(walking == false)
            {
                moveSound.Play();
                walking = true;
            }
        }

        //close animation when stop walking
        if (xAxis == 0)
        {
            moveSound.Stop();
            walking = false;
        }
        //ApplyInput(moveAxis, turnAxis);
        if (Input.GetAxis("Mouse X")> 0)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * rotationRate * Time.deltaTime, 0);
        }

        else if (Input.GetAxis("Mouse X") < 0)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * rotationRate * Time.deltaTime, 0);
        }
    }

    /*private void ApplyInput(float moveInput, float turnInput)
    {
        Move(moveInput);
        Move(turnInput);
    }

    private void Move(float input)
    {
        transform.Translate(Vector3.forward * input * moveSpeed);
        Animating(input, input);
    }

    private void Turn(float input)
    {
        transform.Rotate(0, input * rotationRate * Time.deltaTime, 0);
    }*/
    
    void Animating(float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("isWalking", walking);
    }
}
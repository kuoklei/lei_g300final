﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovement : MonoBehaviour
{
    Transform player;               // Reference to the player's position.
    UnityEngine.AI.NavMeshAgent nav;               // Reference to the nav mesh agent.
    Animator anim;
    float attack_count = 1;
    bool attacking = false;
    bool die = false;
    bool hit = false;
    float timer = 0;
    float attack = 0;
    BoxCollider weaponCollider;
    public GameObject weapon;
    public GameObject bossSoundStore;

    private AudioSource weaponSound;
    private AudioSource bossSound;
    void Awake()
    {
        // Set up the references.
        player = GameObject.FindGameObjectWithTag("Player").transform;
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
        weaponCollider = weapon.GetComponent<BoxCollider>();
        weaponSound = weapon.GetComponent<AudioSource>();
        bossSound = bossSoundStore.GetComponent<AudioSource>();
    }


    void Update()
    {
        timer += Time.deltaTime;

        // open and close the weapon Collider
        if (attack == 1)
        {
            if (timer <= 0.5)
            {
                weaponCollider.enabled = false;
            }
            if (timer > 0.5 && timer <1.0)
            {
                if (hit == false)
                {
                    weaponCollider.enabled = true;
                }
            }
            if (timer > 1.0)
            {
                attacking = false;
                timer = -1;
                weaponCollider.enabled = false;
                hit = false;
                attack = 0;
            }

        }

        if (attack == 2)
        {
            if (timer<=0.5)
            {
                weaponCollider.enabled = false;
            }
            if (timer > 0.5 && timer < 2.75)
            {
                if (hit == false)
                {
                    weaponCollider.enabled = true;
                }
            }
            if (timer > 2.75)
            {
                attacking = false;
                timer = -2;
                weaponCollider.enabled = false;
                hit = false;
                attack = 0;
            }

        }

        // If the enemy and the player have health left...
        if (die == false)
        {
            if (Vector3.Distance(player.position, transform.position) <= 40)
            {
                nav.enabled = true;
                // attack when th eplayer is closing by
                if (Vector3.Distance(player.position, transform.position) <= 6)
                {
                    if (attacking == false)
                    {
                        timer = 0;
                        attacking = true;
                        weaponCollider.enabled = true;
                        // set the attack order, can improve by random number
                        if (attack_count <= 2)
                        {
                            anim.SetTrigger("attack1");
                            attack_count+=1;
                            attack = 1;
                            weaponSound.Play();
                        }
                        if (attack_count >= 3)
                        {
                            anim.SetTrigger("attack2");
                            attack_count = 1;
                            attack = 2;
                            weaponSound.Play();
                            bossSound.Play();
                        }

                    }
                }
                else
                // if player far away, chase them
                {
                    if (Vector3.Distance(player.position, transform.position) <= 40 && Vector3.Distance(player.position, transform.position) >= 10)
                    {
                        nav.speed = 7f;
                        anim.SetBool("running", true);
                        // ... set the destination of the nav mesh agent to the player.
                        nav.SetDestination(player.position);
                    }
                    else
                    {
                        nav.speed = 3.5f;
                        anim.SetBool("running", false);
                        // ... set the destination of the nav mesh agent to the player.
                        nav.SetDestination(player.position);
                    }

                }
            }
            // Otherwise...
            else
            {
                // ... disable the nav mesh agent.
                nav.speed = 3.5f;
                nav.enabled = false;
                anim.SetBool("walking", false);
            }
        }
    }

    //die function 
    public void Die()
    {
        if (die == false)
        {
            die = true;
            print("die");
        }

    }

    //hit 
    public void hitPlayer()
    {
        hit = true;
        print("run");
    }
}

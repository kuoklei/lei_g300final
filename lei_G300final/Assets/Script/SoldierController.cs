﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierController : MonoBehaviour
{
    public GameObject sign;
    public GameObject talkBox;
    public GameObject talkHint;

    bool here = false;
    Transform player;
    Transform GameController;
    GameController GameControll;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        GameController = GameObject.FindGameObjectWithTag("GameController").transform;
        GameControll = GameController.GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        //hint player to talk
        if (Vector3.Distance(player.position, transform.position) <= 5)
        {
            here = true;
            talkHint.SetActive(true);
        }
        if (Vector3.Distance(player.position, transform.position) > 5)
        {
            talkHint.SetActive(false);
        }

        //take to player when player click C
        if (Input.GetKeyDown(KeyCode.C)&& here == true)
        {
            sign.SetActive(false);
            talkBox.SetActive(true);
            talkHint.SetActive(false);
            GameControll.TalkMission();
        }
    }
}

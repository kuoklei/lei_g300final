﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyController : MonoBehaviour
{
    CapsuleCollider bodyCollider;
    public float startHealth;
    public float health;
    public GameObject explosion;
    public float damage;
    public GameObject player;
    public GameObject gameController;
    public Image healthBar;

    PlayerController PlayerController;
    GameController GameController;
    
    // Start is called before the first frame update
    void Awake()
    {
        bodyCollider = gameObject.GetComponent<CapsuleCollider>();
        PlayerController = player.GetComponent<PlayerController>();
        GameController = gameController.GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            GameController.killMummy();
            Destroy(gameObject);
            
        }
    }

    //hit player and hit by player
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "playerWeapon")
        {
            health -= 1;
            healthBar.fillAmount = health / startHealth;
            print("hitMonster");
        }

        if (other.tag == "Player")
        {
            PlayerController.ReduceHealth(damage);
        }
    }

    //continue damage when close to player
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController.ReduceHealth(damage/20);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class invisivle : MonoBehaviour
{
    public GameObject wall;
    MeshRenderer MeshRenderer;
    MeshRenderer MeshRenderer2;

    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer = wall.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //when player's back enter, make invisiable
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "playerBack")
        {
            MeshRenderer.enabled = false;
        }
    }

    //when player left, make it active again
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "playerBack")
        {
            MeshRenderer.enabled = true;
        }
    }
}

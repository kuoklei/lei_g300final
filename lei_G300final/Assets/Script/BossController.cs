﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossController : MonoBehaviour
{
    public float startHealth;
    public float health;
    public GameObject explosion;
    public GameObject spwanDamage;
    public GameObject ten;
    public Image healthBar;

    private float dieTimer = 1;
    private AudioSource bossDieSound;
    private bool die = false;
    Animator anim;
    CapsuleCollider bodyCollider;
    BossMovement BossMovement;
    Transform GameController;
    GameController GameControll;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        bodyCollider = GetComponent<CapsuleCollider>();
        BossMovement = gameObject.GetComponent<BossMovement>();
        bossDieSound = GetComponent<AudioSource>();
        GameController = GameObject.FindGameObjectWithTag("GameController").transform;
        GameControll = GameController.GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        // die when health = 0
        if (health == 0)
        {
            if (die==false)
            {
                bossDieSound.Play();
                Die();
            }
        }

        if(die == true)
        {
            dieTimer += Time.deltaTime;
            if (dieTimer >= 5)
            {
                Destroy(gameObject);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // get hit by player
        if (other.tag == "playerWeapon")
        {
            print("Bosshurt");
            TakeDamage(10);
            Instantiate(ten, spwanDamage.transform.position, transform.rotation);
        }

    }

    private void TakeDamage (float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        
    }

    private void Die()
    {
        GameControll.PlayerWin();
        die = true;
        anim.SetTrigger("die");
        Instantiate(explosion, transform.position, transform.rotation);
        //Destroy(gameObject);
        BossMovement.Die();
        BossMovement.enabled = false;
    }
}

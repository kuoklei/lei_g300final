﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public GameObject player;
    public GameObject emeny;
    public GameObject startText;
    public GameObject endTrigger1;
    public GameObject endTrigger2;
    public Text step;
    public GameObject message;
    public Text message1;

    private AudioSource villageBGM;
    public float mummyCount = 0;
    private bool gameStart;
    private bool endGame = false;
    private bool missioning = false;
    private bool playerDead = false;
    private bool playerWin = false;

    // Start is called before the first frame update
    void Start()
    {
        villageBGM = GetComponent<AudioSource>();
        player.SetActive(false);
        emeny.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Start the game when player click space
        if (Input.GetKeyDown(KeyCode.Space) && gameStart == false)
        {
            startText.SetActive(false);
            player.SetActive(true);
            //emeny.SetActive(true);
            gameStart = true;
            villageBGM.Play();
        }

        //start ending scene when the first mission is done
        if(mummyCount >= 15 && endGame == false)
        {
            endGame = true;
            endTrigger1.SetActive(true);
            endTrigger2.SetActive(true);
        }

        //restart when player die
        if (Input.GetKeyDown(KeyCode.O) && playerDead == true)
        {
            SceneManager.LoadScene("SampleScene");
        }

        //restart when player win
        if (Input.GetKeyDown(KeyCode.O) && playerWin == true)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    public void StopBGM()
    {
        villageBGM.Stop();
    }

    public void PlayBGM()
    {
        villageBGM.Play();
    }

    public void killMummy()
    {
        mummyCount += 1;
        step.text = "Mummy: " + mummyCount.ToString() + " / 15";
    }

    public void TalkMission()
    {
        if(missioning == false)
        {
            missioning = true;
            step.text = "Mummy: " + mummyCount.ToString() + " / 15";
        }
    }

    public void PlayerDead()
    {
        playerDead = true;
        message.SetActive(true);
    }

    public void PlayerWin()
    {
        playerWin = true;
        message1.text = "You save the world, Press 'O' to restart";
        message.SetActive(true);
    }
}

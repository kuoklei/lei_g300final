﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weaponController : MonoBehaviour
{
    CapsuleCollider weaponCollider;
    private AudioSource weaponSound;

    // Start is called before the first frame update
    void Awake()
    {
        weaponCollider = gameObject.GetComponent<CapsuleCollider>();
        weaponSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //when weapon hit enemy
    private void OnTriggerEnter(Collider other)
    {
        print("enter");
        weaponSound.Play();
        if (other.tag=="boss")
        {
            weaponCollider.enabled = false;
        }
        
    }
}

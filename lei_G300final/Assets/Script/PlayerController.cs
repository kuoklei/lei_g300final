﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float startHealth;
    public float health;
    public GameObject weapon;
    public Slider healthBar;
    public GameObject healthFill;
    public GameObject spwanDamage;
    public GameObject twenty;
    public GameObject one;
    public GameObject oneDTwenty;

    Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;
    CapsuleCollider weaponCollider;
    PlayerMovement PlayerMovement;
    Transform GameController;
    GameController GameControll;

    private float dieTimer = 1;
    private float hitTimer = 0;
    private bool die = false;
    float attackTimer = 1;
    bool attacking = false;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        weaponCollider = weapon.GetComponent<CapsuleCollider>();
        PlayerMovement = gameObject.GetComponent<PlayerMovement>();
        GameController = GameObject.FindGameObjectWithTag("GameController").transform;
        GameControll = GameController.GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        hitTimer += Time.deltaTime;
        attackTimer += Time.deltaTime;
        if (attackTimer >= 2)
        {
            attacking = false;
            weaponCollider.enabled = false;
        }
        // get the attack order
        if (Input.GetKeyDown(KeyCode.Mouse0) |Input.GetKeyDown(KeyCode.Space))
        {
            if (attacking == false){
                attacking = true;
                attackTimer = 1;
                weaponCollider.enabled = true;
                attack();
            }

        }

        //die when hp is 0
        if (health <= 0 && die == false) 
        {
            {
                GameControll.PlayerDead();
                Destroy(healthFill);
                Die();
                PlayerMovement.enabled = false;
                this.enabled = false;
            }
        }

        healthBar.value = health / startHealth;
    }

    //get hit by boss weapon
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bossWeapon")
        {
            if (hitTimer>=1.5)
            {
                ReduceHealth(20);
                hitTimer = 0;
            }
            
        }
    }

    //reduce health in purpose
    public void ReduceHealth(float damage)
    {
        health = health - damage;
        if (damage == 20){
            Instantiate(twenty, spwanDamage.transform.position, transform.rotation);
        }
        if (damage == 1)
        {
            Instantiate(one, spwanDamage.transform.position, transform.rotation);
        }
    }

    void attack()
    {
        anim.SetTrigger("Attack");
    }

    private void Die()
    {
        die = true;
        anim.SetTrigger("Die");
        //Destroy(gameObject);
    }
}

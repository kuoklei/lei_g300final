﻿using UnityEngine;
using System.Collections;

public class enemyMovement : MonoBehaviour
{
    public float timer;
    public int newtarget;
    public Vector3 Target;
    public float xRandom1;
    public float xRandom2;
    public float zRandom1;
    public float zRandom2;

    private bool chase = false;
    private AudioSource moveSound;
    Transform player;               // Reference to the player's position.
    UnityEngine.AI.NavMeshAgent nav;               // Reference to the nav mesh agent.


    void Awake()
    {
        // Set up the references.
        player = GameObject.FindGameObjectWithTag("Player").transform;
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        moveSound = GetComponent<AudioSource>();
    }


    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= newtarget && chase == false)
        {
            newTarget();
            timer = 0;
        }
        // If the enemy and the player have health left...
        if (Vector3.Distance(player.position, transform.position) <= 20)
        {
            // ... set the destination of the nav mesh agent to the player.
            nav.enabled = true;
            chase = true;
            moveSound.Play();
            nav.SetDestination(player.position);
        }
        // Otherwise...
        else
        {
            // ... disable the nav mesh agent.
            //nav.enabled = false;
        }
    }

    //change walking direction
    void newTarget()
    {
        float myX = gameObject.transform.position.x;
        float myZ = gameObject.transform.position.z;

        float xPos = Random.Range(xRandom1, xRandom2);
        float zPos = Random.Range(zRandom1, zRandom2);

        Target = new Vector3(myX+xPos, gameObject.transform.position.y, myZ+zPos);

        nav.SetDestination(Target);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bossWeaponController : MonoBehaviour
{
    public GameObject boss;

    BoxCollider weaponCollider;
    PlayerController pc;
    BossMovement BossMovement;
    

    // Start is called before the first frame update
    void Awake()
    {
        weaponCollider = gameObject.GetComponent<BoxCollider>();
        pc = gameObject.GetComponent<PlayerController>();
        BossMovement = boss.GetComponent<BossMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    // when the weapon hurt the player
    private void OnTriggerEnter(Collider other)
    {
        print("hurt");
        BossMovement.hitPlayer();
    }
}

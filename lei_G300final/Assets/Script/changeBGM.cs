﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBGM : MonoBehaviour
{
    public GameObject GameController;

    private AudioSource BGM;
    private AudioSource forestBGM;
    private GameController BGMController;
    private bool enter = false;

    // Start is called before the first frame update
    void Start()
    {
        BGMController = GameController.GetComponent<GameController>();
        forestBGM = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //change BGM when leave the town, or enter the town
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (enter == false){
                print("enter forest");
                BGMController.StopBGM();
                forestBGM.Play();
                enter = true;
            }

            else {
                print("leave forest");
                BGMController.PlayBGM();
                forestBGM.Stop();
                enter = false;
            }
        }

    }

    //used to stop the BGM in purpose
    public void stopBGM()
    {
        forestBGM.Stop();
    }
}

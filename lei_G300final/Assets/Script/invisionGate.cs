﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class invisionGate : MonoBehaviour
{
    public GameObject wall;

    bool frontIn = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        wall.SetActive(true);
    }

    //when player's back enter, make invisiable
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            frontIn = true;
        }
        
        if (other.tag == "playerBack" && frontIn == false)
        {
            wall.SetActive(false);
        }
    }

    //when player left, make it active again
    private void OnTriggerExit(Collider other)
    {
        frontIn = false;
        if (other.tag == "playerBack")
        {
            wall.SetActive(true);
        }
    }
}
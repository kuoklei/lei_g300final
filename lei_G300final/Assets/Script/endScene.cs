﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endScene : MonoBehaviour
{
    public GameObject enemy;
    public GameObject player;
    public GameObject soldier;
    public GameObject deadSoldier;
    public GameObject wall;
    public GameObject GameController;
    public GameObject EndSceneTrigger1;
    public GameObject EndSceneTrigger2;
    public GameObject bossSoundEffect;
    public GameObject invisibleWall;
    public GameObject invisibleWall2;

    BossMovement BossMovement;
    PlayerMovement PlayerMovement;
    private GameController BGMController;
    private changeBGM BGMController2;
    private changeBGM BGMController3;
    private AudioSource endBGM;
    private AudioSource bossSound;
    bool start = false;
    float timer = 0;
    bool frontIn = false;
    // Start is called before the first frame update
    void Start()
    {
        endBGM = GetComponent<AudioSource>();
        bossSound = bossSoundEffect.GetComponent<AudioSource>();
        BossMovement = enemy.GetComponent<BossMovement>();
        PlayerMovement = player.GetComponent<PlayerMovement>();
        BGMController = GameController.GetComponent<GameController>();
        BGMController3 = EndSceneTrigger2.GetComponent<changeBGM>();
        BGMController2 = EndSceneTrigger1.GetComponent<changeBGM>();
    }

    // Update is called once per frame
    void Update()
    {
        if (start == true)
        {
            timer += Time.deltaTime;
            if (timer >= 4 && timer <= 4.3)
            {
                bossSound.Play();
                BossMovement.enabled = true;
                PlayerMovement.enabled = true;
            }
        }

    }

    // start the scene when player walk through the trigger
    private void OnTriggerEnter(Collider other)
    {
        print("end working");
        if (other.tag == "Player" && start == false && enemy.activeSelf == false)
        {
            BGMController.StopBGM();
            BGMController3.stopBGM();
            BGMController2.stopBGM();
            endBGM.Play();
            invisibleWall.SetActive(true);
            invisibleWall2.SetActive(true);
            wall.SetActive(false);
            soldier.SetActive(false);
            PlayerMovement.enabled = false;
            deadSoldier.SetActive(true);
            enemy.SetActive(true);
            start = true;
            bossSound.Play();
        }

        if (other.tag == "Player")
        {
            frontIn = true;
        }
        if (other.tag == "playerBack" && frontIn == false)
        {
            wall.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        frontIn = false;
        wall.SetActive(true);
    }
}
